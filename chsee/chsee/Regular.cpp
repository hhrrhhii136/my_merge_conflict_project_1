#include "Regular.h"

Regular::Regular()
{
}

Regular::Regular(string place, Point placeInBoard)
	: _placeInBoard(placeInBoard)
{
	this->_place = place;
}

void Regular::printSome()
{
	std::cout << this->_place << std::endl;
}


bool Regular::checkMove(Point move)
{
	this->_numOfMoves++;
	//(this->numOfMoves)++;
	if (this->_numOfMoves == 0)
	{
		return
			(((this->_placeInBoard.GetX() - 2) == move.GetX())
				&& ((this->_placeInBoard.GetY()) == move.GetY()))
			|| (((this->_placeInBoard.GetX() - 1) == move.GetX())
				&& ((this->_placeInBoard.GetY()) == move.GetY()));
	}
	else
	{
		return
			(((this->_placeInBoard.GetX() - 1) == move.GetX())
				&& ((this->_placeInBoard.GetY()) == move.GetY()));
	}
}