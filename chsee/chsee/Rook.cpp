#include "Rook.h"

Rook::Rook()
	: PiceControl()
{
}

Rook::Rook(const char& soldierSign, const Point& placeOnBoard, const string& place)
	: PiceControl(soldierSign, placeOnBoard, place)
{
}

Rook::~Rook()
{
}

bool Rook::checkMove(const Point& toGo)
{
	return
		(this->_placeOnBoard.GetX() == toGo.GetX()) || (this->_placeOnBoard.GetY() == toGo.GetY());
}

vector<Point> Rook::piceWay(const Point& toGo)
{
	vector<Point> blocksWay;

	if (this->_placeOnBoard.GetX() == toGo.GetX())
	{
		int xToSet = this->_placeOnBoard.GetX();
		int yToSet = this->_placeOnBoard.GetY();

		if (this->_placeOnBoard.GetY() > toGo.GetY())
		{
			--yToSet;

			while (yToSet >= toGo.GetY())
			{
				blocksWay.push_back(Point(xToSet, yToSet));
				--yToSet;
			}
		}
		else
		{
			++yToSet; 
			
			while (yToSet <= toGo.GetY())
			{
				blocksWay.push_back(Point(xToSet, yToSet));
				++yToSet;
			}
		}

	}

	else
	{
		int xToSet = this->_placeOnBoard.GetX();
		int yToSet = this->_placeOnBoard.GetY();

		if (this->_placeOnBoard.GetX() > toGo.GetX())
		{
			--xToSet;

			while (xToSet >= toGo.GetX())
			{
				blocksWay.push_back(Point(xToSet, yToSet));
			    --xToSet;
			}
		}
		else
		{
			++xToSet;

			while (xToSet <= toGo.GetX())
			{
				blocksWay.push_back(Point(xToSet, yToSet));
				++xToSet;
			}
		}
	}

	return blocksWay;
}