#include "Knight.h"

Knight::Knight()
	: PiceControl()
{
}

Knight::Knight(const char& soldierSign, const Point& placeOnBoard, const string& place)
	: PiceControl(soldierSign, placeOnBoard, place)
{
}

Knight::~Knight()
{
}

bool Knight::checkMove(const Point& toGo)
{
	if (islower(this->_soldierSign))
	{
		return
			((this->_placeOnBoard.GetX() - 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() - 1) == (toGo.GetY()))

			||

			((this->_placeOnBoard.GetX() + 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() - 2) == (toGo.GetY()))

			||

			((this->_placeOnBoard.GetX() - 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() + 1) == (toGo.GetY()))

			||

			((this->_placeOnBoard.GetX() + 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() + 2) == (toGo.GetY()))
			
			||
			
			((this->_placeOnBoard.GetX() - 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() + 2) == (toGo.GetY()))
			
			||

			((this->_placeOnBoard.GetX() - 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() - 2) == (toGo.GetY()))
			
			||
			
			((this->_placeOnBoard.GetX() + 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() + 1) == (toGo.GetY()))
			
			||
			
			((this->_placeOnBoard.GetX() + 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() - 1) == (toGo.GetY()));
	}

	else
	{
		return
			((this->_placeOnBoard.GetX() + 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() + 2) == (toGo.GetY()))

			||

			((this->_placeOnBoard.GetX() + 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() - 2) == (toGo.GetY()))

			||

			((this->_placeOnBoard.GetX() + 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() - 1) == (toGo.GetY()))

			||

			((this->_placeOnBoard.GetX() + 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY() + 1) == (toGo.GetY()));
	}
}

vector<Point> Knight::piceWay(const Point& toGo)
{
	vector<Point> blocksWay; // ���� ����!!!!!!!!!!!!!!!!!!!!

	return blocksWay;
}