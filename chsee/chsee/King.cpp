#include "King.h"

King::King()
	: PiceControl()
{
}

King::King(const char& soldierSign, const Point& placeOnBoard, const string& place)
	: PiceControl(soldierSign, placeOnBoard, place)
{
}

King::~King()
{
}

bool King::checkMove(const Point& toGo)
{
	return
		((this->_placeOnBoard.GetX() - 1) == toGo.GetX() && (this->_placeOnBoard.GetY()) == toGo.GetY())
		||
		((this->_placeOnBoard.GetX() - 1) == toGo.GetX() && (this->_placeOnBoard.GetY() + 1) == toGo.GetY())
		||
		((this->_placeOnBoard.GetX()) == toGo.GetX() && (this->_placeOnBoard.GetY() + 1) == toGo.GetY())
		||
		((this->_placeOnBoard.GetX() + 1) == toGo.GetX() && (this->_placeOnBoard.GetY() + 1) == toGo.GetY())
		||
		((this->_placeOnBoard.GetX() + 1) == toGo.GetX() && (this->_placeOnBoard.GetY()) == toGo.GetY())
		||
		((this->_placeOnBoard.GetX() + 1) == toGo.GetX() && (this->_placeOnBoard.GetY() - 1) == toGo.GetY())
		||
		((this->_placeOnBoard.GetX()) == toGo.GetX() && (this->_placeOnBoard.GetY() - 1) == toGo.GetY())
		||
		((this->_placeOnBoard.GetX() - 1) == toGo.GetX() && (this->_placeOnBoard.GetY() - 1) == toGo.GetY());
}

vector<Point> King::piceWay(const Point& toGo)
{
	vector<Point> blocksWay;
	
	blocksWay.push_back(toGo);

	return blocksWay;
}