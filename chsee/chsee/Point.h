#pragma once

#ifndef POINT_H
#define POINT_H


#include <string>
#include <iostream> 

class Point
{
private:

	int _x;
	int _y;

public:
	Point();
	Point(const Point& other);
	Point(double n, double t);
	virtual ~Point();

	//Getters
	int GetX() const;
	int GetY() const;

	//Setters
	virtual void SetX(const double n);
	virtual void SetY(const double t);

	//Point convertString(const std::string place);

	//void printDetails(const std::string msgFromMsg);

	// operators
	Point& operator=(const Point& other);
	Point& operator+=(const Point& other);
	bool operator==(const Point& other);

};

Point convertString(const std::string place);
void printDetails(const std::string msgFromMsg);
#endif