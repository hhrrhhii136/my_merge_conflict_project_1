#include "MainGame.h"

MainGame::MainGame()
{
}

MainGame::~MainGame()
{
}


map<string, PiceControl*>* MainGame::getAllPices()
{
	return &(this->allPices);
}

void MainGame::deletePice(string key)
{
	this->allPices.erase(key);
}

void MainGame::addPice(string oldKey, string newKey)
{
	PiceControl* temp(this->allPices[oldKey]);

	temp->SetPlace(newKey);
	temp->SetPlaceOnBoard(convertString(newKey));


	deletePice(oldKey);
	this->allPices.insert(std::pair<string, PiceControl*>(newKey, temp));
}


void MainGame::GameControl(string msgFromGame)
{
	string picePlace;
	string piceToGo;

	picePlace += msgFromGame[0];
	picePlace += msgFromGame[1];

	piceToGo += msgFromGame[2];
	piceToGo += msgFromGame[3];

	Point picePlacePoint = convertString(picePlace);
	Point piceToGoPoint = convertString(piceToGo);

	if (board[picePlacePoint.GetX()][picePlacePoint.GetY()] == '#')
		throw MoveExeption(2);
	else if(board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] == '#')
		throw MoveExeption(3);

	else if ((picePlacePoint.GetX() < 0 || picePlacePoint.GetX() > 7)
		|| (picePlacePoint.GetY() < 0 || picePlacePoint.GetY() > 7)
		|| (piceToGoPoint.GetX() < 0 || piceToGoPoint.GetX() > 7)
		|| (piceToGoPoint.GetX() < 0 || piceToGoPoint.GetY() > 7))
		throw MoveExeption(5);

	else if (picePlacePoint == piceToGoPoint)
		throw MoveExeption(7);


}