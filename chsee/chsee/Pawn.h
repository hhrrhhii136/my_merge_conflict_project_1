#pragma once

#include "PiceControl.h"

class Pawn : public PiceControl
{
public:
	Pawn();
	Pawn(const char& soldierSign, const Point& placeOnBoard, const string& place);
	virtual ~Pawn();

	virtual bool checkMove(const Point& toGo);
	virtual vector<Point> piceWay(const Point& toGo);

private:
	int numsOfMoves = 0;
};