#include "Board.h"

Board::Board()
{}

void Board::print()
{
	cout << "\n";
	for (int i = 0; i < LEN_OF_BOARD; i++)
	{
		for (int j = 0; j < LEN_OF_BOARD; j++)
		{
			cout << this->board[i][j] << " ";
		}
		cout << "\n";
	}
}

void Board::toMovePice(const Point& picePlace, const Point& toMove)
{
	board[toMove.GetX()][toMove.GetY()] = board[picePlace.GetX()][picePlace.GetY()];
	board[picePlace.GetX()][picePlace.GetY()] = '#';
}