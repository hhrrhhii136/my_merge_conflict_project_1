#pragma once
#include <iostream>
#include "Point.h"

using std::cout;
#define LEN_OF_BOARD 8

class Board
{


public:
	Board();
	void print();
	void toMovePice(const Point& picePlace, const Point& toMove);


	
protected:
	char board[LEN_OF_BOARD][LEN_OF_BOARD] = 
	{ { 'R', 'N', 'B', 'K', 'Q', 'B', 'N', 'R' },
	  { 'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P' },
	  { '#', '#', '#', '#', '#', '#', '#', '#' },
	  { '#', '#', '#', '#', '#', '#', '#', '#' },
	  { '#', '#', '#', '#', '#', '#', '#', '#' },
	  { '#', '#', '#', '#', '#', '#', '#', '#' },
	  { 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p' },
	  { 'r', 'n', 'b', 'k', 'q', 'b', 'n', 'r' } };
};